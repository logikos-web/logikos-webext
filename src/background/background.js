var config = {};

browser.browserAction.onClicked.addListener(function() {
    browser.tabs.create({'url':config.url});
})

browser.storage.local.get("config").then(data => {
    if (data.config) {
        config = data.config;
    } else {
        config = {url: "http://localhost:8080/logikos"};
        browser.storage.local.set({config});
    }

});

browser.storage.onChanged.addListener((change, area) => {
    if (area == "local" && change.config) {
        config.url = change.config.newValue.url;
        console.log(config);
    }
});

