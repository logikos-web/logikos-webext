browser.storage.local.get('config').then(data => {
  var config = data.config;
  if (config) {
      document.getElementById("logikos-url").value = config.url;
  };
});

var saveConfiguration = function() {
  var config = {};
  config.url = document.getElementById("logikos-url").value;
  browser.storage.local.set({config});
};

document.getElementById("submit-button").addEventListener("click", event => {
  saveConfiguration();
});